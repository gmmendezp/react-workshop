import axios from 'axios';
import types from './types';

export default () => axios.get(
  `${process.env.REACT_APP_SERVER}/items`,
).then(response => ({
  type: types.GET_ITEMS,
  items: response.data.data
}));
