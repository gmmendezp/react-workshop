import axios from 'axios';
import types from './types';

export default (item) => axios.post(
  `${process.env.REACT_APP_SERVER}/items`,
  { data: item }
).then(() => ({
  type: types.ADD_ITEM,
  item
}));
