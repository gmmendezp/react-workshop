import { StyleSheet } from 'aphrodite';

const styles = {
  app: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%'
  }
};

export default StyleSheet.create(styles);
