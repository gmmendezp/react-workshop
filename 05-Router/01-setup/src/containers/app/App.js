import React, { Component } from 'react';
import { connect } from 'react-redux';
import { css } from 'aphrodite';
import {
  addItemAction,
  removeItemAction,
  getItemsAction
} from '../../actions';
import List from '../../components/list/List';
import MyForm from '../../components/myform/MyForm';
import styles from './App.styles';

class App extends Component {
  componentWillMount() {
    this.props.getItems();
  }
  render() {
    return (
      <div className={css(styles.app)}>
        <MyForm onSubmit={this.props.addItem} />
        <List data={this.props.items} onRemove={this.props.removeItem} />
      </div>
    );
  }
}

function mapStateToProps({ items }) {
  return { items: items.list };
};

const mapDispatchToProps = dispatch => {
  return {
    addItem() {
      dispatch(addItemAction(...arguments));
    },
    removeItem() {
      dispatch(removeItemAction(...arguments));
    },
    getItems() {
      dispatch(getItemsAction(...arguments));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
