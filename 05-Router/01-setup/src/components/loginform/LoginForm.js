import React, { Component } from 'react';
import { css } from 'aphrodite';
import Button from '../button/Button';
import Input from '../input/Input';
import styles from './LoginForm.styles';

class LoginForm extends Component {
  state = {
    user: '',
    password: ''
  }

  setUser = ({ target: { value } }) => this.setState({ user: value })
  setPassword = ({ target: { value } }) => this.setState({ password: value })

  onSubmit = (e) => {
    e.preventDefault();
    this.props.onSubmit(this.state);
    this.setState({ user: '', password: '' });
  }

  render() {
    return (
      <form onSubmit={this.onSubmit} className={css(styles.form)}>
        <div className={css(styles.title)}>Login</div>
        <Input
          style={styles.formItems}
          type="text"
          value={this.state.user}
          onChange={this.setUser}
        />
        <Input
          style={styles.formItems}
          type="password"
          value={this.state.password}
          onChange={this.setPassword}
        />
        <Button style={styles.formItems} value='Send' />
      </form>
    )
  }
}

export default LoginForm;
