// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from '../containers/login/Login';
import App from '../containers/app/App';

class Router extends Component<Props> {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Route path="/login" component={Login} />
          <Route exact path="/" component={App} />
        </div>
      </BrowserRouter>
    );
  }
}

function mapStateToProps({ }) {
  return { };
}

export default connect(mapStateToProps)(Router);
