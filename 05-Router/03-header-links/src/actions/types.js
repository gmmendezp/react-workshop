export default {
  ADD_ITEM: 'Add an item',
  REMOVE_ITEM: 'Remove an item',
  GET_ITEMS: 'Retrieve al items'
};
