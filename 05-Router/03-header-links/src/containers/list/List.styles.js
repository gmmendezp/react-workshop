import { StyleSheet } from 'aphrodite';

const styles = {
  list: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%'
  },
  add: {
    padding: 10,
    alignSelf: 'flex-start'
  }
};

export default StyleSheet.create(styles);
