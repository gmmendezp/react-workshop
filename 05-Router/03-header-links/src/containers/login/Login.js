import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoginForm from '../../components/loginform/LoginForm';

class App extends Component {
  handleSubmit = (values) => {
    console.log(values);
  }
  render() {
    return (
      <LoginForm onSubmit={this.handleSubmit}/>
    );
  }
};

const mapDispatchToProps = dispatch => {
  return {

  };
};

export default connect(null, mapDispatchToProps)(App);
