import React, { Component } from 'react';
import { connect } from 'react-redux';
import { css } from 'aphrodite';
import styles from './Header.styles';
import Button from '../../components/button/Button';
import LoginForm from '../../components/loginform/LoginForm';

class App extends Component {
  logout = () => {

  };
  render() {
    return (
      <header className={css(styles.header)}>
        <span className={css(styles.logo)}>Header</span>
        <div className={css(styles.actions)}>
          <Button value="Logout" onClick={this.logout}/>
        </div>
      </header>
    );
  }
};

const mapDispatchToProps = dispatch => {
  return {

  };
};

export default connect(null, mapDispatchToProps)(App);
