import { StyleSheet } from 'aphrodite';
import { css } from 'aphrodite';

const styles = {
  header: {
    display: 'flex',
    alignItems: 'center',
    height: '60px',
    width: '100%',
    backgroundColor: '#666',
    color: 'white',
    padding: 15,
    boxSizing: 'border-box'
  },
  logo: {
    fontSize: 18,
    fontWeight: 'bold',
    marginRight: 'auto'
  }
};

export default StyleSheet.create(styles);
