import { StyleSheet } from 'aphrodite';

const styles = {
  button: {
    cursor: 'pointer',
    fontWeight: 600,
    fontSize: '1em',
    color: '#666',
    padding: 10,
    height: 35,
    width: '100%',
    border: '1px solid #bbb',
    borderRadius: 10,
    boxSizing: 'border-box',
    ':hover': {
      backgroundColor: '#bbb'
    }
  }
};

export default StyleSheet.create(styles);
