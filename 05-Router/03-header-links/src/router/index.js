// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Add from '../containers/add/Add';
import Header from '../containers/header/Header';
import List from '../containers/list/List';
import Login from '../containers/login/Login';

class Router extends Component<Props> {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/" component={Header} />
          </Switch>
          <Route exact path="/" component={List} />
          <Route exact path="/add" component={Add} />
        </div>
      </BrowserRouter>
    );
  }
}

function mapStateToProps({ }) {
  return { };
}

export default connect(mapStateToProps)(Router);
