// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Add from '../containers/add/Add';
import Header from '../containers/header/Header';
import List from '../containers/list/List';
import Login from '../containers/login/Login';
import PrivateRoute from './PrivateRoute';

class Router extends Component<Props> {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Switch>
            <Route
              path="/login"
              render={(props) =>
                this.props.isAuthenticated ?
                  <Redirect to="/" /> :
                  <Login {...props}/>
              }
            />
            <Route path="/" component={Header} />
          </Switch>
          <PrivateRoute exact
            path="/"
            component={List}
            isAuthenticated={this.props.isAuthenticated}
          />
          <PrivateRoute
            path="/add"
            component={Add}
            isAuthenticated={this.props.isAuthenticated}
          />
        </div>
      </BrowserRouter>
    );
  }
}

function mapStateToProps({ auth: { isAuthenticated } }) {
  return { isAuthenticated };
}

export default connect(mapStateToProps)(Router);
