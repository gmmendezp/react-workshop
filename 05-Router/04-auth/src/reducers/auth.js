import { types as actions } from '../actions';
const defaultState = {
  isAuthenticated: false,
  user: null,
  loginError: false
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.LOGIN_SUCCESS:
      return { isAuthenticated: true, user: action.user };
    case actions.LOGOUT_SUCCESS:
      return { isAuthenticated: false, user: null };
    default:
      return state;
  }
};
