import { StyleSheet } from 'aphrodite';

const styles = {
  title: {
    margin: '0 0 10px',
    textTransform: 'uppercase',
    fontWeight: 'bold',
    color: '#bbb'
  },
  form: {
    maxWidth: 400,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    padding: 20
  },
  formItems: {
    margin: '2px 0'
  }
};

export default StyleSheet.create(styles);
