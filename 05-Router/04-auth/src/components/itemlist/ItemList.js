import React from 'react';
import { css } from 'aphrodite';
import styles from './ItemList.styles';

const renderItem = (item, index, onRemove) => {
  return (
    <li key={index} className={css(styles.item)}>
      <span className={css(styles.text)}>{item}</span>
      <span className={css(styles.close)} onClick={() => onRemove(index)}>
        ✖
      </span>
    </li>
  )
};

const ItemList = (props) => {
  if(!props.data || !props.data.length) {
    return false;
  }
  return (
    <ul className={css(styles.list)}>
      {props.data.map(
        (item, index) => renderItem(item, index, props.onRemove)
      )}
    </ul>
  )
}

export default ItemList;
