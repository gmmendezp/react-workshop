export default {
  ADD_ITEM: 'Add an item',
  REMOVE_ITEM: 'Remove an item',
  GET_ITEMS: 'Retrieve al items',
  LOGIN_SUCCESS: 'Successful log in',
  LOGIN_FAIL: 'Error during log in',
  LOGOUT_SUCCESS: 'Successful log out',
};
