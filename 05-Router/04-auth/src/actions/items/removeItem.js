import axios from 'axios';
import types from '../types';

export default (index) => axios.get(
  `${process.env.REACT_APP_SERVER}/items/${index}`,
).then(() => ({
  type: types.REMOVE_ITEM,
  index
}));
