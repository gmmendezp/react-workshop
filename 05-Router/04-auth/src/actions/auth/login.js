import types from '../types';

export default ({ user, password }) => {
  if(user === 'user1' && password === '1234') {
    return {
      type: types.LOGIN_SUCCESS,
      user
    };
  } else {
    return {
      type: types.LOGIN_FAIL
    }
  }
};
