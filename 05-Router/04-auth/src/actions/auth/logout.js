import types from '../types';

export default () => ({
  type: types.LOGOUT_SUCCESS
});
