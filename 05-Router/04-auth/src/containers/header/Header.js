import React, { Component } from 'react';
import { connect } from 'react-redux';
import { css } from 'aphrodite';
import styles from './Header.styles';
import { logoutAction } from '../../actions';
import Button from '../../components/button/Button';
import LoginForm from '../../components/loginform/LoginForm';

class App extends Component {
  render() {
    return (
      <header className={css(styles.header)}>
        <span className={css(styles.logo)}>Header</span>
        <div className={css(styles.actions)}>
          <Button value="Logout" onClick={this.props.logout}/>
        </div>
      </header>
    );
  }
};

const mapDispatchToProps = dispatch => {
  return {
    logout() {
      dispatch(logoutAction());
    }
  };
};

export default connect(null, mapDispatchToProps)(App);
