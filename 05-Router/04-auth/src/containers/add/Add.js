import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  addItemAction
} from '../../actions';
import { css } from 'aphrodite';
import styles from './Add.styles';
import MyForm from '../../components/myform/MyForm';

class Add extends Component {
  render() {
    return (
      <div className={css(styles.add)}>
        <Link to='/' className={css(styles.back)}>Back to list</Link>
        <MyForm onSubmit={this.props.addItem} />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addItem() {
      dispatch(addItemAction(...arguments));
    }
  };
};

export default connect(null, mapDispatchToProps)(Add);
