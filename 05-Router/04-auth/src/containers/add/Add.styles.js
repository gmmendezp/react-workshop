import { StyleSheet } from 'aphrodite';

const styles = {
  add: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%'
  },
  back: {
    padding: 10,
    alignSelf: 'flex-start'
  }
};

export default StyleSheet.create(styles);
