import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loginAction } from '../../actions';
import { css } from 'aphrodite';
import styles from './Login.styles';
import LoginForm from '../../components/loginform/LoginForm';

class App extends Component {
  render() {
    return (
      <div className={css(styles.login)}>
        <LoginForm onSubmit={this.props.login}/>
      </div>
    );
  }
};

const mapDispatchToProps = dispatch => {
  return {
    login() {
      dispatch(loginAction(...arguments))
    }
  };
};

export default connect(null, mapDispatchToProps)(App);
