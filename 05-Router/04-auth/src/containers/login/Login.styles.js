import { StyleSheet } from 'aphrodite';
import { css } from 'aphrodite';

const styles = {
  login: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%'
  }
};

export default StyleSheet.create(styles);
