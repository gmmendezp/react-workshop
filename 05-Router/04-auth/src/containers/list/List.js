import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  removeItemAction
} from '../../actions';
import { css } from 'aphrodite';
import styles from './List.styles';
import ItemList from '../../components/itemlist/ItemList';

class List extends Component {
  render() {
    return (
      <div className={css(styles.list)}>
        <Link to="/add" className={css(styles.add)}>Add new item</Link>
        <ItemList data={this.props.items} onRemove={this.props.removeItem} />
      </div>
    );
  }
}

function mapStateToProps({ items }) {
  return { items: items.list };
};

const mapDispatchToProps = dispatch => {
  return {
    removeItem() {
      dispatch(removeItemAction(...arguments));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(List);
