import types from './types';
import axios from 'axios';

const removeItem = (index) => ({
  type: types.REMOVE_ITEM,
  index
});

const removeItemAsync = (index) => {
  return dispatch => {
    axios.delete(`${process.env.REACT_APP_SERVER}/items/${index}`)
      .then(response => dispatch(removeItem(index)))
  };
};

export default removeItemAsync;
