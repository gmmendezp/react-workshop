import axios from 'axios';
import types from './types';

const addItem = (item) => ({
  type: types.ADD_ITEM,
  item
});

const addItemAsync = (item) => {
  return dispatch => {
    axios.post(`${process.env.REACT_APP_SERVER}/items`, { data: item })
      .then(response => dispatch(addItem(item)))
  }
};

export default addItemAsync;
