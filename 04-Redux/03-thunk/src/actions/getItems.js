import types from './types';
import axios from 'axios';

const getItems = (items) => ({
  type: types.GET_ITEMS,
  items
});

const getItemsAsync = () => {
  return dispatch => {
    axios.get(`${process.env.REACT_APP_SERVER}/items`).then(response => {
      dispatch(getItems(response.data.data));
    })
  };
};

export default getItemsAsync;
