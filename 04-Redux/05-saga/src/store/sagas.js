import axios from 'axios';
import { put, takeEvery } from 'redux-saga/effects';
import {
  types as actions,
  setItemsAction,
  addedItemAction,
  removedItemAction
} from '../actions';

function* getItems() {
  let response = yield axios.get(`${process.env.REACT_APP_SERVER}/items`);
  yield put(setItemsAction(response.data.data));
}

function* addItem({ item }) {
  yield axios.post(`${process.env.REACT_APP_SERVER}/items`, { data: item });
  yield put(addedItemAction(item));
}

function* removeItem({ index }) {
  yield axios.delete(`${process.env.REACT_APP_SERVER}/items/${index}`);
  yield put(removedItemAction(index));
}

export function* itemsSaga() {
  yield takeEvery(actions.GET_ITEMS, getItems);
  yield takeEvery(actions.ADD_ITEM, addItem);
  yield takeEvery(actions.REMOVE_ITEM, removeItem);
}
