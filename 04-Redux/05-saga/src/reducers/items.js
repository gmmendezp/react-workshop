import { types as actions } from '../actions';
const defaultState = {
  list: []
};

export default (state = defaultState, action) => {
  let list = [...state.list];
  switch (action.type) {
    case actions.ADD_ITEM:
      list.push(action.item);
      return { list };
    case actions.REMOVE_ITEM:
      list.splice(action.index, 1);
      return { list };
    case actions.SET_ITEMS:
      return {
        list: action.items
      }
    default:
      return state;
  }
};
