import types from './types';

export default (index) => ({
  type: types.REMOVED_ITEM,
  index
});
