export default {
  ADD_ITEM: 'Add an item',
  ADDED_ITEM: 'Added an item',
  REMOVE_ITEM: 'Remove an item',
  REMOVED_ITEM: 'Removed an item',
  GET_ITEMS: 'Retrieve al items',
  SET_ITEMS: 'Set list of items'
};
