import types from './types';

export default (items) => ({ type: types.SET_ITEMS, items });
