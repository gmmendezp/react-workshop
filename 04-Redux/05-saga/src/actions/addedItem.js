import types from './types';

export default (item) => ({
  type: types.ADDED_ITEM,
  item
});
