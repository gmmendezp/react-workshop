import React from 'react';
import { css } from 'aphrodite';
import styles from './Input.styles';

const Input = (props) => {
  const { value, onChange, style, ...rest } = props;
  return (
    <input
      value={value}
      onChange={onChange}
      className={css(styles.input, style)}
      {...rest}
    />
  );
}

export default Input;
