import { ofType } from 'redux-observable';
import { ajax } from 'rxjs/observable/dom/ajax';
import { map } from 'rxjs/operators/map';
import { switchMap } from 'rxjs/operators/switchMap';
import { types as actions, removedItemAction } from '../actions';

export default function(action$) {
  let index;
  return action$.pipe(
    ofType(actions.REMOVE_ITEM),
    switchMap(action => {
      index = action.index;
      return ajax.delete(`${process.env.REACT_APP_SERVER}/items/${index}`);
    }),
    map(data => removedItemAction(index))
  );
}
