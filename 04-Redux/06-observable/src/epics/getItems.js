import { ofType } from 'redux-observable';
import { ajax } from 'rxjs/observable/dom/ajax';
import { map } from 'rxjs/operators/map';
import { switchMap } from 'rxjs/operators/switchMap';
import { types as actions, setItemsAction } from '../actions';

export default function(action$) {
  return action$.pipe(
    ofType(actions.GET_ITEMS),
    switchMap(action => ajax.get(`${process.env.REACT_APP_SERVER}/items`)),
    map(({ response }) => setItemsAction(response.data))
  );
}
