import { combineEpics } from 'redux-observable';
import addItemEpic from './addItem';
import removeItemEpic from './removeItem';
import getItemsEpic from './getItems';

const rootEpic = combineEpics(addItemEpic, removeItemEpic, getItemsEpic);

export default rootEpic;
