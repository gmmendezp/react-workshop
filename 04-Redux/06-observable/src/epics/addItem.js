import { ofType } from 'redux-observable';
import { ajax } from 'rxjs/observable/dom/ajax';
import { map } from 'rxjs/operators/map';
import { switchMap } from 'rxjs/operators/switchMap';
import { types as actions, addedItemAction } from '../actions';

export default function(action$: Observable, store: Store) {
  let item;
  return action$.pipe(
    ofType(actions.ADD_ITEM),
    switchMap(action => {
      item = action.item;
      return ajax.post(
        `${process.env.REACT_APP_SERVER}/items`,
        { data: item }
      );
    }),
    map(data => addedItemAction(item))
  );
}
