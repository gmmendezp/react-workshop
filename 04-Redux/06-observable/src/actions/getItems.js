import types from './types';

export default () => ({ type: types.GET_ITEMS });
