import types from './types';

export default (index) => ({
  type: types.REMOVE_ITEM,
  index
});
