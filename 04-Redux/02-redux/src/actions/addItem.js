import types from './types';

export default (item) => ({
  type: types.ADD_ITEM,
  item
});
