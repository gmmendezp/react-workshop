import React from 'react';
import { css } from 'aphrodite';
import styles from './Button.styles';

const Button = (props) => {
  const { value, style, ...rest } = props;
  return (
    <button
      className={css(styles.button, style)}
      {...rest}
    >
      {value}
    </button>
  );
}

export default Button;
