import React, { Component } from 'react';
import { css } from 'aphrodite';
import List from './components/list/List';
import MyForm from './components/myform/MyForm';
import styles from './App.styles';

class App extends Component {
  state = {
    data: []
  }
  onSubmit = (value) => {
    this.setState({ data: [...this.state.data, value] });
  }
  onRemove = (index) => {
    let data = [...this.state.data];
    data.splice(index, 1);
    this.setState({ data });
  }
  render() {
    return (
      <div className={css(styles.app)}>
        <MyForm onSubmit={this.onSubmit} />
        <List data={this.state.data} onRemove={this.onRemove} />
      </div>
    );
  }
}

export default App;
