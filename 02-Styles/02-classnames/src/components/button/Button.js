import React from 'react';
import './Button.css';

const Button = (props) => {
  const { value, className, ...rest } = props;
  return (
    <button
      className={`button ${className}`}
      {...rest}
    >
      {value}
    </button>
  );
}

export default Button;
