import React, { Component } from 'react';
import Button from '../button/Button';
import Input from '../input/Input';
import './MyForm.css';

class MyForm extends Component {
  state = {
    value: 'Hello, world!'
  }

  setValue = ({ target: { value } }) => this.setState({ value })

  onSubmit = (e) => {
    e.preventDefault();
    this.setState({ value: '' });
    this.props.onSubmit(this.state.value);
  }

  render() {
    return (
      <form onSubmit={this.onSubmit} className="form">
        <div className="form-title">Form</div>
        <Input
          className="form-item"
          type="text"
          value={this.state.value}
          onChange={this.setValue}
        />
        <Button className="form-item" value="Send" />
      </form>
    )
  }
}

export default MyForm;
