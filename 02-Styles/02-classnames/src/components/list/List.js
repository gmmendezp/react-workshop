import React from 'react';
import './List.css';

const renderItem = (item, index, onRemove) => {
  return (
    <li key={index} className="list-item">
      <span className="item-text">{item}</span>
      <span className="item-close" onClick={() => onRemove(index)}>
        ✖
      </span>
    </li>
  )
};

const List = (props) => {
  if(!props.data || !props.data.length) {
    return false;
  }
  return (
    <ul className="list">
      {props.data.map(
        (item, index) => renderItem(item, index, props.onRemove)
      )}
    </ul>
  )
}

export default List;
