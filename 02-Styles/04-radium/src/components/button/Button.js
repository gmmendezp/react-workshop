import React from 'react';
import Radium from 'radium';
import styles from './Button.styles';

const Button = (props) => {
  const { value, style, ...rest } = props;
  return (
    <button
      style={[styles.button, style]}
      {...rest}
    >
      {value}
    </button>
  );
}

export default Radium(Button);
