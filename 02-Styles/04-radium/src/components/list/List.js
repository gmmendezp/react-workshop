import React from 'react';
import Radium from 'radium';
import styles from './List.styles';

const renderItem = (item, index, onRemove) => {
  return (
    <li key={index} style={styles.item}>
      <span style={styles.text}>{item}</span>
      <span style={styles.close} onClick={() => onRemove(index)}>
        ✖
      </span>
    </li>
  )
};

const List = (props) => {
  if(!props.data || !props.data.length) {
    return false;
  }
  return (
    <ul style={styles.list}>
      {props.data.map(
        (item, index) => renderItem(item, index, props.onRemove)
      )}
    </ul>
  )
}

export default Radium(List);
