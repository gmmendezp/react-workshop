import React, { Component } from 'react';
import Radium from 'radium';
import Button from '../button/Button';
import Input from '../input/Input';
import styles from './MyForm.styles';

class MyForm extends Component {
  state = {
    value: 'Hello, world!'
  }

  setValue = ({ target: { value } }) => this.setState({ value })

  onSubmit = (e) => {
    e.preventDefault();
    this.setState({ value: '' });
    this.props.onSubmit(this.state.value);
  }

  render() {
    return (
      <form onSubmit={this.onSubmit} style={styles.form}>
        <div style={styles.title}>Form</div>
        <Input
          style={styles.formItems}
          type="text"
          value={this.state.value}
          onChange={this.setValue}
        />
        <Button style={styles.formItems} value='Send' />
      </form>
    )
  }
}

export default Radium(MyForm);
