import React from 'react';
import Radium from 'radium';
import styles from './Input.styles';

const Input = (props) => {
  const { value, onChange, style, ...rest } = props;
  return (
    <input
      value={value}
      onChange={onChange}
      style={[styles.input, style]}
      {...rest}
    />
  );
}

export default Radium(Input);
