import React from 'react';
import './Input.css'

const Input = (props) => {
  const { value, onChange, className, ...rest } = props;
  return (
    <input
      value={value}
      onChange={onChange}
      className={`input ${className}`}
      {...rest}
    />
  );
}

export default Input;
