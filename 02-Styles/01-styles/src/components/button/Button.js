import React from 'react';


const styles = {
  button: {
    fontWeight: 600,
    fontSize: '1em',
    color: '#666',
    padding: 10,
    height: 35,
    width: '100%',
    border: '1px solid #bbb',
    borderRadius: 10,
    boxSizing: 'border-box'
  }
};

const Button = (props) => {
  const { value, style, ...rest } = props;
  return (
    <button
      style={Object.assign({}, styles.button, style)}
      {...rest}
    >
      {value}
    </button>
  );
}

export default Button;
