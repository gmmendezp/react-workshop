import React from 'react';

const styles = {
  input: {
    fontWeight: 600,
    fontSize: '1em',
    color: '#666',
    padding: 10,
    height: 35,
    width: '100%',
    border: '1px solid #bbb',
    borderRadius: 10,
    boxSizing: 'border-box'
  }
};

const Input = (props) => {
  const { value, onChange, style, ...rest } = props;
  return (
    <input
      value={value}
      onChange={onChange}
      style={Object.assign({}, styles.input, style)}
      {...rest}
    />
  );
}

export default Input;
