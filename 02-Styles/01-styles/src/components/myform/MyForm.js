import React, { Component } from 'react';
import Button from '../button/Button';
import Input from '../input/Input';

class MyForm extends Component {
  state = {
    value: 'Hello, world!'
  }

  styles = {
    title: {
      margin: '0 0 10px',
      textTransform: 'uppercase',
      fontWeight: 'bold',
      color: '#bbb'
    },
    form: {
      maxWidth: 400,
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      padding: 20
    },
    formItems: {
      margin: '2px 0'
    }
  }

  setValue = ({ target: { value } }) => this.setState({ value })

  onSubmit = (e) => {
    e.preventDefault();
    this.setState({ value: '' });
    this.props.onSubmit(this.state.value);
  }

  render() {
    return (
      <form onSubmit={this.onSubmit} style={this.styles.form}>
        <div style={this.styles.title}>Form</div>
        <Input
          style={this.styles.formItems}
          type="text"
          value={this.state.value}
          onChange={this.setValue}
        />
        <Button style={this.styles.formItems} value='Send' />
      </form>
    )
  }
}

export default MyForm;
