import React from 'react';

const styles = {
  list: {
    display: 'flex',
    flexDirection: 'column',
    minWidth: 500,
    maxWidth: 800,
    width: '100%',
    backgroundColor: '#bbb',
    boxShadow: '0px 8px 16px 0px rgba(0,0,0,0.2)',
    borderRadius: 8,
    marginTop: 8,
    padding: '10px 0'
  },
  item: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: 0,
    width: '100%',
    boxSizing: 'border-box'
  },
  text: {
    padding: '0 10px',
    maxWidth: '90%',
    overflow: 'hidden'
  },
  close: {
    padding: '0 10px',
    cursor: 'pointer'
  }
};

const renderItem = (item, index, onRemove) => {
  return (
    <li key={index} style={styles.item}>
      <span style={styles.text}>{item}</span>
      <span style={styles.close} onClick={() => onRemove(index)}>
        ✖
      </span>
    </li>
  )
};

const List = (props) => {
  if(!props.data || !props.data.length) {
    return false;
  }
  return (
    <ul style={styles.list}>
      {props.data.map(
        (item, index) => renderItem(item, index, props.onRemove)
      )}
    </ul>
  )
}

export default List;
