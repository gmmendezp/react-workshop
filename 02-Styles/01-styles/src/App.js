import React, { Component } from 'react';
import List from './components/list/List';
import MyForm from './components/myform/MyForm';
import './App.css';

class App extends Component {
  state = {
    data: []
  }
  onSubmit = (value) => {
    this.setState({ data: [...this.state.data, value] });
  }
  onRemove = (index) => {
    let data = [...this.state.data];
    data.splice(index, 1);
    this.setState({ data });
  }
  render() {
    return (
      <div className="App">
        <MyForm onSubmit={this.onSubmit} />
        <List data={this.state.data} onRemove={this.onRemove} />
      </div>
    );
  }
}

export default App;
