import React from 'react';
import MyForm from './MyForm';
import { shallow, mount, render } from 'enzyme';

it('renders without crashing', () => {
  shallow(<MyForm />);
});

it('renders class correctly', () => {
  let wrapper = shallow(<MyForm />);

  expect(wrapper.hasClass('form'));

  expect(wrapper).toHaveClassName('form');
});

it('call onSubmit function correctly', () => {
  let onSubmitMock = jest.fn();
  let wrapper = shallow(<MyForm onSubmit={onSubmitMock}/>);

  wrapper.simulate('submit', { preventDefault: () => {}});
  expect(onSubmitMock.mock.calls.length).toBe(1);
});

it('update state correctly', () => {
  let wrapper = mount(<MyForm />);

  let newValue = 'test-value';
  let input = wrapper.find('input');
  input.simulate('change', {target: {value: newValue }});
  expect(wrapper.state().value).toBe(newValue);
  expect(wrapper).toHaveState('value', newValue);
});

it('update input value correctly', (done) => {
  let wrapper = mount(<MyForm />);
  let newValue = 'test-value';
  wrapper.setState({ 'value': newValue }, () => {
    expect(wrapper.find('input').props().value).toBe(newValue);
    expect(wrapper.find('input')).toHaveProp('value', newValue);
    done();
  });
});

it('renders correct title', () => {
  let wrapper = render(<MyForm />);
  expect(wrapper.find('.form-title').text()).toBe('Form');

  wrapper = shallow(<MyForm />);
  expect(wrapper.find('.form-title').text()).toBe('Form');
});
