import React from 'react';
import ReactDOM from 'react-dom';
import MyForm from './MyForm';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MyForm />, div);

  expect(div.childNodes[0].className).toBe('form');

  ReactDOM.unmountComponentAtNode(div);
});
