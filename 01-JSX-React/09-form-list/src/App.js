import React, { Component } from 'react';
import List from './components/list/List';
import MyForm from './components/myform/MyForm';
import './App.css';

class App extends Component {
  state = {
    data: []
  }
  onSubmit = (value) => {
    this.setState({ data: [...this.state.data, value] });
  }
  render() {
    return (
      <div className="App">
        <MyForm onSubmit={this.onSubmit} />
        <List data={this.state.data} />
      </div>
    );
  }
}

export default App;
