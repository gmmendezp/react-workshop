import React, { Component } from 'react';
import Button from '../button/Button';
import Input from '../input/Input';

class MyForm extends Component {
  state = {
    value: 'Hello, world!'
  }

  setValue = ({ target: { value } }) => this.setState({ value })

  onSubmit = (e) => {
    e.preventDefault();
    this.setState({ value: '' });
    this.props.onSubmit(this.state.value);
  }

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <Input type="text" value={this.state.value} onChange={this.setValue}/>
        <Button value='Send' />
      </form>
    )
  }
}

export default MyForm;
