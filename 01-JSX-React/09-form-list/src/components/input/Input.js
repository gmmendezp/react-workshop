import React from 'react';

const Input = (props) => {
  const { value, onChange, ...rest } = props;
  return <input value={value} onChange={onChange} {...rest}/>
}

export default Input;
