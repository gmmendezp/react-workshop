import React from 'react';

const renderItem = (item, index) => {
  return (
    <li key={index}>
      {item}
    </li>
  )
};

const List = (props) => {
  return (
    <ul>
      {props.data.map(renderItem)}
    </ul>
  )
}

export default List;
