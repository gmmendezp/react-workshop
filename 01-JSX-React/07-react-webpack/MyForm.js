import React, { Component } from 'react';

function Input(props) {
  return <input {...props}/>
}

function Button(props) {
  return <button onClick={props.onClick}>{props.value}</button>
}

class MyForm extends Component {
  state = {
    value: "Hello, world!"
  }

  setValue = ({ target: { value } }) => this.setState({ value })

  clickHandler = () => {
    console.log(this.state.value);
    alert(this.state.value);
  }

  render() {
    return (
      <form>
        <Input type="text" value={this.state.value} onChange={this.setValue}/>
        <Button onClick={this.clickHandler} value='Send' />
      </form>
    )
  }
}

export default MyForm;
