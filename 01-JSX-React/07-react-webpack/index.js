import React from 'react';
import ReactDOM from 'react-dom';
import MyForm from './MyForm';

ReactDOM.render(
  <MyForm />,
  document.getElementById('root')
);
