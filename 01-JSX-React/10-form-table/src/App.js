import React, { Component } from 'react';
import Table from './components/table/Table';
import MyForm from './components/myform/MyForm';
import { typeValues } from './constants';
import './App.css';

class App extends Component {
  state = {
    data: []
  }
  typeOptions = [
    { value: '', text: 'Select' },
    ...Object.keys(typeValues).map(
      (type) => ({ value: type, text: typeValues[type] })
    )
  ]

  onSubmit = (formData) => {
    this.setState({ data: [...this.state.data, formData] });
  }
  onRemove = (index) => {
    let data = [...this.state.data];
    data.splice(index, 1);
    this.setState({ data });
  }
  render() {
    return (
      <div className="App">
        <MyForm onSubmit={this.onSubmit} typeOptions={this.typeOptions} />
        <Table
          data={this.state.data}
          onRemove={this.onRemove}
          typeValues={typeValues}
        />
      </div>
    );
  }
}

export default App;
