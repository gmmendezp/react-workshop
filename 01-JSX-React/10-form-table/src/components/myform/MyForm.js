import React, { Component } from 'react';
import Button from '../button/Button';
import Field from '../field/Field';

class MyForm extends Component {
  state = {
    user: '',
    type: '',
    approved: false,
    startDate: '',
    endDate: ''
  }

  setValue = (key, { target: { type, checked, value } }) => {
    if(type === 'checkbox') {
      value = checked;
    }
    this.setState({ [key]:  value })
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.setState({
      user: '',
      type: '',
      approved: false,
      startDate: '',
      endDate: ''
    });
    this.props.onSubmit(this.state);
  }

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <Field
          label="User: "
          type="text"
          value={this.state.user}
          onChange={(e) => this.setValue('user', e)}
        />
        <Field
          label="Type: "
          type="select"
          value={this.state.type}
          options={this.props.typeOptions}
          onChange={(e) => this.setValue('type', e)}
        />
        <Field
          label="Approved: "
          type="checkbox"
          value={this.state.approved}
          onChange={(e) => this.setValue('approved', e)}
        />
        <Field
          label="Start Date: "
          type="text"
          value={this.state.startDate}
          onChange={(e) => this.setValue('startDate', e)}
        />
        <Field
          label="End Date: "
          type="text"
          value={this.state.endDate}
          onChange={(e) => this.setValue('endDate', e)}
        />
        <Button value='Send' />
      </form>
    )
  }
}

export default MyForm;
