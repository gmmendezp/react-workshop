import React, { Component } from 'react';

class Table extends Component {
  renderItem = (item, index) => {
    return (
      <tr key={index}>
        <td>{item.user}</td>
        <td>{item.startDate}</td>
        <td>{item.endDate}</td>
        <td>{this.props.typeValues[item.type]}</td>
        <td>{item.approved ? 'Approved' : 'Not approved'}</td>
        <td onClick={() => this.props.onRemove(index)}>✖</td>
      </tr>
    )
  };
  render() {
    return (
      <table>
        <thead>
          <tr>
            <th>User</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Type</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {this.props.data.map(this.renderItem)}
        </tbody>
      </table>
    )
  }
}

export default Table;
