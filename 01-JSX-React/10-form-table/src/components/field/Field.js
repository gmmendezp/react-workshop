import React from 'react';
import Input from '../input/Input';

const Field = (props) => {
  const { value, onChange, label, type, options, ...rest } = props;
  let input = <Input type={type} value={value} onChange={onChange} {...rest} />;
  if(type === 'select') {
    input = (
      <select value={value} onChange={onChange} {...rest}>
        {options.map((item, index) =>
          <option key={index} value={item.value}>{item.text}</option>
        )}
      </select>
    )
  }
  return (
    <div>
      <label>{label}</label>
      {input}
    </div>
  );
}

export default Field;
