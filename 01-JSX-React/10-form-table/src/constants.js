export const typeValues = {
  vacation: 'Vacation Time',
  sickleave: 'Sick Leave',
  maternity: 'Maternity or Paternity Leave',
  bereavement: 'Bereavement Leave',
  marriage: 'Marriage License',
  withoutpay: 'Leave without Pay',
  other: 'Other'
}
